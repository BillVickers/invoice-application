﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ISFInvoice.Startup))]
namespace ISFInvoice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
