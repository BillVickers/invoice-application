﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISFInvoice.Models
{
    public class Invoice
    {
        [Key]
        public int InvoiceId { get; set; }
        [Display(Name = "Customer Name")]
        [Required(ErrorMessage = "The customer name is required")]
        [StringLength(40, ErrorMessage = "The customer name can not be more than 40 characters long")]
        public string CustomerName { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int InvoiceCount { get; set; }
        [DataType(DataType.Currency)]
        [Range(1, 1000000, ErrorMessage = "Price must be between $1 and $1,000,000")]
        [Display(Name = "Invoice Amount")]
        [Required(ErrorMessage = "The invoice amount is required")]
        [RegularExpression(@"^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$", ErrorMessage = "Amount is invalid.")]
        public decimal? InvoiceAmount { get; set; }
        public decimal? InvoiceAmountTotal { get; set; }

        [ForeignKey("CreatedByUser")]
        public virtual string CreatedById { get; set; }
        public ApplicationUser CreatedByUser { get; set; }
    }
}
