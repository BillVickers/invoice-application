namespace ISFInvoice.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ISFInvoice.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ISFInvoice.Models.ApplicationDbContext context)
        {
            context.Invoices.AddOrUpdate(

               new Invoice

               {
                   InvoiceId = 1,
                   CustomerName = "ISF",
                   InvoiceDate = DateTime.Parse("2/4/2016"),
                   InvoiceAmount = 1000,
                   InvoiceCount = 1,
                   InvoiceAmountTotal = 1000
               }

        );

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
